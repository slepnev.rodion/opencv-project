import numpy as np
import cv2
import time

vehicle_classifier = cv2.CascadeClassifier('cars.xml')
cap = cv2.VideoCapture()


# The best among others
cap.open("http://149.43.156.105/mjpg/video.mjpg")




while cap.isOpened():
    # time.sleep(2)
    # Capture frame-by-frame
    try:
        ret, frame = cap.read()
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        cars = vehicle_classifier.detectMultiScale(gray, 1.1, 3)
     
        for (x,y,w,h) in cars:
            cv2.rectangle(frame,(x,y),(x+w,y+h),(0,0,255),2)

        # Display the resulting frame
        cv2.imshow('frame',frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
              break
    except:
        continue

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()
